package xmlfile;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Valentin Ros Duart
 */
public class ReadXML {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Instance of DocumentBuilder to create the parser
        DocumentBuilder documentBuilder;
        try {
            documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            // Process the XML File and create Document object
            Document doc;
            try {
                doc = documentBuilder.parse(new File(".\\res\\AndroidManifest.xml"));
                // Get root node
                Element rootElement = doc.getDocumentElement();

                // Get all child nodes from root node
                NodeList childs = rootElement.getChildNodes();
                // Iterate childs
                for(int i=0; i < childs.getLength(); i++){
                    Node node = childs.item(i);
                    if (node instanceof Element){
                        System.out.print("<" + node.getNodeName() + " ");
                        if (node.getAttributes() != null) {
                            for(int j=0; j < node.getAttributes().getLength(); j++) {
                                System.out.print(node.getAttributes().item(j).toString());
                            }
                            System.out.print("></" + node.getNodeName() + ">");
                        }
                        System.out.println("");
                    }
                }
            } catch (SAXException ex) {
                Logger.getLogger(ReadXML.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ReadXML.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(ReadXML.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
}
