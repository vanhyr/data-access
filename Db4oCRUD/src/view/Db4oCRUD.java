package view;

import controller.Db4oDB;
import model.Client;

/**
 * Stablish connection to Db4oCRUD and perform CRUD.
 *
 * @author Valentin Ros Duart.
 */
public class Db4oCRUD {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Db4oDB db4o = new Db4oDB("db4o.db");
        Client c1 = new Client("15471059V", "Valentin", "Ros Duart", "Murillo 3");
        Client c2 = new Client("88191576P", "Alicia", "Navas Gomez", "Greco 19");
        // In order to search all objects of that class (table), make it equal to null
        Client client = null;
        //System.out.println(db4o.insert(c1));
        //System.out.println(db4o.insert(c2));
        //System.out.println(db4o.exists(c1));
        //System.out.println(db4o.exists(c2));
        System.out.println(db4o.delete(c1));
        //System.out.println(db4o.update(c1));
        System.out.println(db4o.search(client));
    }

}
