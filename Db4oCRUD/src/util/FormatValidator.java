package util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

/**
 * Class for controlling data format.
 * @author Valentin Ros Duart.
 */
public class FormatValidator {
    
    /**
     * Check if the String passed can be parsed to LocalDate with given format.
     * @param localDate the date string.
     * @param pattern the pattern to check against.
     * @return true if can be parsed, false if not.
     */
    public static boolean isValidLocalDate(String localDate, String pattern) {
        try {
            LocalDate.parse(localDate, DateTimeFormatter.ofPattern(pattern));
        } catch (DateTimeParseException ex) {
            return false;
        }
        return true;
    }
    
    /**
     * Check if the String passed can be parsed to LocalDate with "dd/MM/yyyy" format.
     * @param localDate the date string.
     * @return true if can be parsed, false if not.
     */
    public static boolean isValidLocalDate(String localDate) {
        try {
            LocalDate.parse(localDate, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        } catch (DateTimeParseException ex) {
            return false;
        }
        return true;
    }
    
    /**
     * Check if the String passed can be parsed to LocalTime with given format.
     * @param localTime the time string.
     * @param pattern the pattern to check against.
     * @return true if can be parsed, false if not.
     */
    public static boolean isValidLocalTime(String localTime, String pattern) {
        try {
            LocalTime.parse(localTime, DateTimeFormatter.ofPattern(pattern));
        } catch (DateTimeParseException ex) {
            return false;
        }
        return true;
    }
    
    /**
     * Check if the String passed can be parsed to LocalTime with "HH:mm" format.
     * @param localTime the time string.
     * @return true if can be parsed, false if not.
     */
    public static boolean isValidLocalTime(String localTime) {
        try {
            LocalTime.parse(localTime, DateTimeFormatter.ofPattern("HH:mm"));
        } catch (DateTimeParseException ex) {
            return false;
        }
        return true;
    }
    
    /**
     * Check if the String passed can be parsed to LocalDateTime with given format.
     * @param localDateTime the datetime string.
     * @param pattern the pattern to check against.
     * @return true if can be parsed, false if not.
     */
    public static boolean isValidLocalDateTime(String localDateTime, String pattern) {
        try {
            LocalDateTime.parse(localDateTime, DateTimeFormatter.ofPattern(pattern));
        } catch (DateTimeParseException ex) {
            return false;
        }
        return true;
    }
    
    /**
     * Check if the String passed can be parsed to LocalDateTime with
     * "dd/MM/yyyy HH:mm" format.
     * @param localDateTime the date string.
     * @return true if can be parsed, false if not.
     */
    public static boolean isValidLocalDateTime(String localDateTime) {
        try {
            LocalDateTime.parse(localDateTime, DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm"));
        } catch (DateTimeParseException ex) {
            return false;
        }
        return true;
    }
    
    /**
     * Check if the String passed is a valid DNI.
     * Checks that firts eight digits are numbers and if the letter is valid.
     * @param dni
     * @return true if it's valid, false if not.
     */
    public static boolean isValidDni(String dni) {
        if (dni.length() != 9 || !Character.isLetter(dni.charAt(8))) {
            return false;
        }
        String numbers = dni.substring(0, 8);
        String letter = dni.substring(8).toUpperCase();
        return validateNumbers(numbers) && generateLetter(numbers).equals(letter);
    }
    
    /**
     * Validate if only numbers are present.
     * @param numbers firts eight digits of a dni.
     * @return true if all are numbers, false if not.
     */
    private static boolean validateNumbers(String numbers) {
        /*for (int i = 0; i < numbers.length(); i++) {
            try {
                if (!Character.isDigit(numbers.charAt(i))) {
                    return false;
                }
            } catch (IndexOutOfBoundsException ex) {
                return false;
            }
        }
        return true;*/
        try {
            Integer.parseInt(numbers);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }
    
    /**
     * Generates a dni letter from the first eight digits of a dni.
     * @param numbers firts eight characters of the dni.
     * @return the dni letter or null if fails.
     */
    private static String generateLetter(String numbers) {
        final String letters = "TRWAGMYFPDXBNJZSQVHLCKE";
        try {
            return Character.toString(letters.charAt(Integer.parseInt(numbers) % 23));
        } catch (NumberFormatException | IndexOutOfBoundsException ex) {
            return null;
        }
    }
    
}
