package controller;

import com.db4o.Db4oEmbedded;
import com.db4o.EmbeddedObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.ext.DatabaseClosedException;
import com.db4o.ext.DatabaseFileLockedException;
import com.db4o.ext.DatabaseReadOnlyException;
import com.db4o.ext.Db4oIOException;
import com.db4o.ext.IncompatibleFileFormatException;
import com.db4o.ext.OldFormatException;
import java.util.ArrayList;


/**
 * Class for connection to DB4O db.
 * 
 * There are 2 ways to access db4o:
 * 1) Embedded (file) I'm using this one.
 * 2) Server-Client (idk)
 * 
 * Note that the connection with the file must be opened and closed for every
 * operation.
 * 
 * @author Valentin Ros Duart
 */
public class Db4oDB {
    
    EmbeddedObjectContainer embeddedObjectContainer;
    String fileUri;

    public Db4oDB(String fileUri) {
        this.fileUri = fileUri;
    }
    
    /**
     * Open the db embeddedObjectContainer for the file uri.
     * @return true if it could be opened, false if not.
     */
    public boolean open() {
        try {
            // Create the embeddedObjectContainer (embedded) and specify the db file.
            embeddedObjectContainer = Db4oEmbedded.openFile(fileUri);
            return true;
        } catch (DatabaseFileLockedException | DatabaseReadOnlyException
                | Db4oIOException | IncompatibleFileFormatException
                | OldFormatException ex) {
            System.out.println(ex.getMessage());
        }
        return false;
    }
    
    /**
     * Inserts an object into the db if it doesn't exists.
     * @param object the object.
     * @return true if it could insert, false if not.
     */
    public boolean insert(Object object) {
        if (!exists(object)) {
            if (open()) {
                try {
                    embeddedObjectContainer.store(object);
                    close();
                    return true;
                } catch (DatabaseReadOnlyException | DatabaseClosedException ex) {
                    System.out.println(ex.getMessage());
                    close();
                    return false;
                }
            }
        }
        return false;
    }
    
    /**
     * Updates an object from the db, if it exists.
     * @param object the object.
     * @return true if it could update, false if not.
     */
    public boolean update(Object object) {
        if (exists(object)) {
            if (open()) {
                try {
                    embeddedObjectContainer.store(object);
                    close();
                    return true;
                } catch (DatabaseReadOnlyException | DatabaseClosedException ex) {
                    System.out.println(ex.getMessage());
                    close();
                    return false;
                }
            }
        }
        return false;
    }
    
    /**
     * Searchs objects from the db.
     * @param object the objects, null to search all objects of that type.
     * @return the object we searched if exists. Null if not.
     */
    public Object search(Object object) {
        ArrayList<Object> objects = new ArrayList<>();
        if (open()) {
            try {
                ObjectSet objectSet = embeddedObjectContainer.queryByExample(object);
                while (objectSet.hasNext()) {
                    objects.add(objectSet.next());
                }
                close();
                return objects;
            } catch (Db4oIOException | DatabaseClosedException ex) {
                System.out.println(ex.getMessage());
                close();
                return null;
            }
        }
        return null;
    }
    
    /**
     * Check if an object exists in the db.
     * @param object the object.
     * @return true if the object exists. False if not.
     */
    public boolean exists(Object object) {
        if (open()) {
            try {
                ObjectSet objectSet = embeddedObjectContainer.queryByExample(object);
                //System.out.println(objectSet.get(0));
                System.out.println(objectSet.size());
                //if (!objectSet.isEmpty()) {
                if (objectSet.size() > 0) {
                    close();
                    return true;
                }
                close();
            } catch (Db4oIOException | DatabaseClosedException ex) {
                System.out.println(ex.getMessage());
                close();
                return false;
            }
        }
        return false;
    }
    
    
    
    /**
     * Deletes an object from the db if it exists.
     * @param object the object.
     * @return true if it could delete, false if not.
     */
    public boolean delete(Object object) {
        if (exists(object)) {
            if (open()) {
                try {
                    embeddedObjectContainer.delete(object);
                    close();
                    return true;
                } catch (Db4oIOException | DatabaseReadOnlyException | DatabaseClosedException ex) {
                    System.out.println(ex.getMessage());
                    close();
                    return false;
                }
            }
        }
        return false;
    }
    
    /**
     * Close the db connection. (idk if it should be boolean)
     * @return true if it could be closed, false if not.
     */
    public boolean close() {
        try {
            embeddedObjectContainer.close();
            return true;
        } catch(Db4oIOException ex) {
            System.out.println(ex.getMessage());
        }
        return false;
    }
    
}
