package model;

import util.FormatValidator;

/**
 * Model class client.
 * @author Valentin Ros Duart.
 */
public class Client {
    
    private String dni, name, surname, address;

    public Client(String dni, String name, String surname, String address) {
        this.dni = dni;
        this.name = name;
        this.surname = surname;
        this.address = address;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        if (FormatValidator.isValidDni(dni)) {
            this.dni = dni;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Client{" + "dni=" + dni + ", name=" + name + ", surname=" + surname + ", address=" + address + '}';
    }
    
}
