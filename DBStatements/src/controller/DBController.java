package controller;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Column;
import model.Table;

/**
 * Class to manage data from db.
 * @author Valentin Ros Duart.
 */
public class DBController {
    
    private final Connection connection;
    
    /**
     * Constructor, initializes the connection.
     * @param odb
     */
    public DBController(OracleDB odb) {
        connection = odb.getConnection();
    }
    
    /**
     * Retrieve tables from db, stores them in a Table object and adds it to the ArrayList.
     * @return user tables from the db for class connection.
     */
    public ArrayList<Table> getTables() {
        ArrayList<Table> tables = new ArrayList<>();
        // Retrieve all tables from the connection that are user tables and order
        String query = "SELECT table_name FROM user_tables ORDER BY table_name";
        try {
            Statement statement = connection.createStatement();
            try {
                ResultSet rs = statement.executeQuery(query);
                while (rs.next()) {
                    // Get the name from resultSet and the column from method below (using metadata)
                    String tableName = rs.getString(1);
                    tables.add(new Table(tableName, getColumnsByMetadata(tableName)));
                }
                // Close resultSet
                rs.close();
            } catch (SQLTimeoutException ex) {
                System.out.println(ex.getSQLState());
            }
            // Close statement
            statement.close();
        } catch (SQLException ex) {
            System.out.println(ex.getSQLState());
        }
        return tables;
    }
    
    /**
     * Retrieve tables from db, stores the in a Table object and adds it to the ArrayList.
     * -- Used metadata but can't filter by user tables so it doesn't work as intended --
     * @return user tables from the db for class connection.
     */
    /*public ArrayList<Table> getTablesByMetadata() {
        ArrayList<Table> tables = new ArrayList<>();
        try {
            // Get the metadata
            DatabaseMetaData metaData = connection.getMetaData();
            // Only get type table
            String[] types = {"TABLE"};
            // Pass the type
            ResultSet rs = metaData.getTables(null, null, "%", types);
            while (rs.next()) {
                // 3 is the table name from the resultSet
                String tableName = rs.getString(3);
                tables.add(new Table(tableName, getColumnsByMetadata(tableName)));
            }
            rs.close();
        } catch (SQLException ex) {
            System.out.println(ex.getSQLState() + ex.getMessage());
        }
        return tables;
    }*/
    
    /**
     * Retrieves the columns from a given table name in the connection.
     * @param tableName the table name.
     * @return columns from the table.
     */
    private ArrayList<Column> getColumnsByMetadata(String tableName) {
        ArrayList<Column> columns = new ArrayList<>();
        try {
            // Get metadata
            DatabaseMetaData metaData = connection.getMetaData();
            // Get resultSet from that metadata
            ResultSet rs = metaData.getColumns(null, null, tableName.toUpperCase(), null);
            while (rs.next()) {
                columns.add(new Column(rs.getString(4), rs.getString(6)));
            }
            rs.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage() + ex.getSQLState());
        }
        return columns;
    }
    
    /**
     * Retrieves the columns from a given table name in the connection.
     * -- Used manual query but can't get it to work --
     * @param tableName the table name.
     * @return columns from the table.
     */
    /*private ArrayList<Column> getColumns(String tableName) {
        ArrayList<Column> columns = new ArrayList<>();
        // Tried to pass the tableName and do a prepStatement
        String query = "SELECT column_name, data_type FROM user_tab_cols WHERE table_name = '?'";
        PreparedStatement prepStatement;
        try {
            // Prepare the statement
            prepStatement = connection.prepareStatement(query);
            // Give it the ?  value (the tableName)
            prepStatement.setString(1, tableName.toUpperCase());
            try {
                // Execute the query
                ResultSet rs = prepStatement.executeQuery();
                while (rs.next()) {
                    // I try to pass the column object the name and data type but it fails
                    // -- Error might be here --
                    columns.add(new Column(rs.getString(1), rs.getString(2)));
                }
                rs.close();
            } catch (SQLTimeoutException ex) {
                System.out.println(ex.getSQLState());
            }
            prepStatement.close();
        } catch (SQLException ex) {
            System.out.println(ex.getSQLState() + ex.getMessage());
        }
        return columns;
    }*/
    
    /**
     * Executes given query.
     * @param query the query to execute.
     * @return a list containing the header and the rows from the connection.
     */
    public ArrayList<ArrayList> execute(String query) {
        ArrayList<ArrayList> result = new ArrayList<>();
        // To store table headers
        ArrayList<String> header = new ArrayList<>();
        // To store table rows
        ArrayList<ArrayList> rows = new ArrayList<>();
        try {
            // Documentation says "If the same SQL statement is executed many 
            // times, it may be more efficient to use a PreparedStatement object."
            Statement statement = connection.createStatement();
            try {
                // Get resultSet from query
                ResultSet rs = statement.executeQuery(query);
                // Get resultSetMetadata from above resultSet
                ResultSetMetaData rsmd = rs.getMetaData();
                // Get the number of colums of that resultSet
                int columnCount = rsmd.getColumnCount();
                // Iterate the number of columns
                for (int i = 1; i <= columnCount; i++) {
                    // Add to header each columName for each column
                    header.add(rsmd.getColumnName(i));
                }
                // Iterate the resultset
                while (rs.next()) {
                    // List for storing the current row data
                    ArrayList<String> row = new ArrayList<>();
                    // Iterate the number of columns
                    for (int i = 1; i <= columnCount; i++) {
                        // For each row store the value for all the columns (dynamically)
                        row.add(rs.getString(i));
                    }
                    rows.add(row);
                }
                // Close resultSet
                rs.close();
            } catch (SQLTimeoutException ex) {
                System.out.println(ex.getSQLState());
            }
            // Close the statement
            statement.close();
            // Add to the output the header and the rows for that query
            result.add(header);
            result.add(rows);
        } catch (SQLException ex) {
            System.out.println(ex.getSQLState());
        } catch (NullPointerException ex) {
            System.out.println(ex.getMessage());
        }
        return result;
    }
    
}
