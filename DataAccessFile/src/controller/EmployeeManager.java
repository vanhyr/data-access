package controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 * Class EmployeeManager which stores and interacts with Employee objects
 * @author Valentin Ros Duart
 */
public class EmployeeManager {
    
    private ArrayList employees;
    
    public EmployeeManager() {
        this.employees = new ArrayList();
    }
    
    public EmployeeManager(ArrayList employees) {
        this.employees = employees;
    }
    
    /**
     * 
     * @return employees ArrayList
     */
    public ArrayList getEmployees() {
        try {
            return employees;
        } catch(NullPointerException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }
    
    /**
     *
     * @param employee
     * @return res boolean wheter it succeds or not
     */
    public boolean addToArrayList(Employee employee) {
        boolean res = false;
        if(!employees.isEmpty()) {
            boolean isUnique = false;
            for(int i = 0; i < employees.size(); i++) {
                Employee empl = (Employee) employees.get(i);
                System.out.println(empl.getId() + " " + employee.getId() + "(" + isUniqueId(empl, employee) + ")");
                if(isUniqueId(empl, employee)) {
                    employees.add(employee);
                    System.out.println("inserted");
                }
            }
            
        } else {
            employees.add(employee);
        }
        return res;
    }
    
    public boolean isUniqueId(Employee employee1, Employee employee2) {
        boolean isUnique = false;
        if(employee1.getId() != employee2.getId()) {
            isUnique = true;
        }
        return isUnique;
    }
    
    public static void main(String[] args) {
        //TEST
        String dateString = "10/05/1996";
        LocalDate date = LocalDate.parse(dateString, DateTimeFormatter.ofPattern("dd/MM/uuuu"));
        Employee e1 = new Employee(1, "pepe", 1.2, date, "web", 18);
        Employee e2 = new Employee(2, "daniel", 1.4, date, "irc", 30);
        Employee e3 = new Employee(2, "alicia", 1.3, date, "asd", 40);
        //System.out.println(e1.toString());
        EmployeeManager em = new EmployeeManager();
        em.addToArrayList(e1);
        em.addToArrayList(e2);
        em.addToArrayList(e3);
        //System.out.println(em.isUniqueId(e3, e2));
        System.out.println(em.getEmployees());
    }
}
