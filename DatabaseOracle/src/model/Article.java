package model;

import java.util.ArrayList;

/**
 *
 * @author Valentin Ros Duart
 */
public class Article {
    
    private String code;
    private String name;
    private Double price;
    
    public Article() {
        code = "";
        name = "";
        price = 0.0;
    }
    
    public Article(String code, String name, Double price) {
        this.code = code;
        this.name = name;
        this.price = price;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        if (price > 0) {
            this.price = price;
        }
    }

    @Override
    public String toString() {
        return "Article {" + "code=" + code + ", name=" + name + ", price=" + price + '}';
    }
    
    public ArrayList<Article> toArrayList(){
        ArrayList article = new ArrayList();
        article.add(code);
        article.add(name);
        article.add(price);
        return article;
    }
    
}
