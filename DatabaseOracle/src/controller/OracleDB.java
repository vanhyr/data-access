package controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Valentin Ros Duart
 */
public class OracleDB {
    
    private Connection connection;
    
    public OracleDB() {
        // Is it better to init the connection in the constructor or in a connect() method?
        /*try {
            connection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "manager");
        } catch (SQLException ex) {
            System.out.println(ex.getSQLState());
        }*/
    }
    
    // I don't know about this...
    public OracleDB(String server, int port, String id, String user, String pass) {
        try {
            connection = DriverManager.getConnection("jdbc:oracle:thin:@" + server + ":" + port + ":" + id, user, pass);
        } catch (SQLException ex) {
            System.out.println(ex.getSQLState());
        }
    }
    
    public Connection connect() {
        try {
            return connection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "manager");
        } catch (SQLException ex) {
            System.out.println(ex.getSQLState());
            return null;
        }
    }
    
    public Connection connect(String server, int port, String id, String user, String pass) {
        try {
            return connection = DriverManager.getConnection("jdbc:oracle:thin:@" + server + ":" + port + ":" + id, user, pass);
        } catch (SQLException ex) {
            System.out.println(ex.getSQLState());
            return null;
        }
    }
    
    public Connection getConnection() {
        return connection;
    }
    
    public boolean close() {
        try {
            connection.close();
            return true;
        } catch (SQLException ex) {
            System.out.println(ex.getErrorCode());
            return false;
        }
    }
    
}
