package controller;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Article;

/**
 *
 * @author Valentin Ros Duart
 */
public class ArticleManager {
    
    private OracleDB oracleDB;

    public ArticleManager() {
        String uri;
        switch (System.getProperty("os.name")) {
            case "Linux":
                uri = "./res/credentials.txt";
            break;
            case "Windows":
                uri = ".\\res\\credentials.txt";
            break;
            default:
                uri = "./res/credentials.txt";
            break;
        }
        FileManager fileManager = new FileManager(uri);
        ArrayList<String> credentials = fileManager.readLines();
        try {
            oracleDB = new OracleDB(credentials.get(0), Integer.parseInt(credentials.get(1)),
                        credentials.get(2), credentials.get(3), credentials.get(4));
        } catch (IndexOutOfBoundsException ex) {
            System.out.println(ex.getMessage());
        }
    }
    
    public ArrayList<Article> getArticles() {
        ArrayList<Article> articles = new ArrayList<>();
        String query = "SELECT referencia, descripcion, precio FROM articulos";
        try {
            // Documentation says "If the same SQL statement is executed many 
            // times, it may be more efficient to use a PreparedStatement object."
            Statement statement = oracleDB.getConnection().createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                articles.add(new Article(resultSet.getString(1), resultSet.getString(2), resultSet.getDouble(3)));
            }
            resultSet.close();
            statement.close();
        } catch (SQLException ex) {
            System.out.println(ex.getSQLState());
        } catch (NullPointerException ex) {
            System.out.println(ex.getMessage());
        }
        return articles;
    }
    
    public int insertArticle(Article article) {
        String query = "INSERT INTO articulos (referencia, descripcion, precio) VALUES (?, ?, ?)";
        int affectedRows = 0;
        try {
            PreparedStatement preparedStatement = oracleDB.getConnection().prepareStatement(query);
            preparedStatement.setString(1, article.getCode());
            preparedStatement.setString(2, article.getName());
            preparedStatement.setDouble(3, article.getPrice());
            try {
                affectedRows = preparedStatement.executeUpdate();
            } catch (SQLTimeoutException ex) {
                System.out.println(ex.getSQLState());
            }
            preparedStatement.close();
        } catch (SQLException ex) {
            System.out.println(ex.getSQLState());
        }
        return affectedRows;
    }

    public OracleDB getOracleDB() {
        return oracleDB;
    }    
 
}
