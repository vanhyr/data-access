package model;

import util.FormatValidator;

/**
 * Model class for the db Festive table.
 * @author Valentin Ros Duart
 */
public class Festive {
    
    private String date;

    public Festive(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        if (FormatValidator.isValidLocalDate(date)) {
            this.date = date;
        }
    }

    @Override
    public String toString() {
        return "Festive{" + "date=" + date + '}';
    }
    
}
