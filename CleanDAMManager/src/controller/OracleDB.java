package controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Manage Oracle db connections.
 * @author Valentin Ros Duart.
 */
public class OracleDB {
    
    private Connection connection;
    
    /**
     * Default constructor
     */
    public OracleDB() {
        
    }

    /**
     * Constructor for the given connection values
     * @param server server uri.
     * @param port port number.
     * @param id id or edition.
     * @param user username.
     * @param pass password.
     */
    public OracleDB(String server, int port, String id, String user, String pass) {
        try {
            connection = DriverManager.getConnection("jdbc:oracle:thin:@" + server + ":" + port + ":" + id, user, pass);
        } catch (SQLException ex) {
            System.out.println(ex.getSQLState());
        }
    }
    
    /**
     * Connects to the default db.
     * @return the connection or null if it couldn't connect.
     */
    public Connection connect() {
        try {
            return connection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "manager");
        } catch (SQLException ex) {
            System.out.println(ex.getSQLState());
            return null;
        }
    }
    
    /**
     * Connects to the db for the given connection values.
     * @param server server uri.
     * @param port port number.
     * @param id id or edition.
     * @param user username.
     * @param pass password.
     * @return the connection or null if it couldn't connect.
     */
    public Connection connect(String server, int port, String id, String user, String pass) {
        try {
            return connection = DriverManager.getConnection("jdbc:oracle:thin:@" + server + ":" + port + ":" + id, user, pass);
        } catch (SQLException ex) {
            System.out.println(ex.getSQLState());
            return null;
        }
    }
    
    /**
     * Get current connection.
     * @return the connection.
     */
    public Connection getConnection() {
        return connection;
    }
    
    /**
     * Close the connection.
     * @return true if it was succesfully closed.
     * false if it couldn't be closed.
     */
    public boolean close() {
        try {
            connection.close();
            return true;
        } catch (SQLException ex) {
            System.out.println(ex.getSQLState());
            return false;
        }
    }
    
}
