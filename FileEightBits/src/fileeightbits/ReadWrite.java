package fileeightbits;

import java.io.*;

/**
 *
 * @author Valentin
 */
public class ReadWrite {
    
    // ObjectInputStream (siguiente que vamos a ver) [writeObject()/readObject()]
    // ObjectInputStream(f,true)
    // Al cambiar el contenido de un objeto y querer escribirlo de nuevo hay que crear un nuevo objeto
        // oos.writeObject(new Ord("hp"))
    // EJERCICIO
        // Clase del objeto
            // implements serializable
        // Clase del flujo de objetos (escribir los objetos)
    public static void main(String[] args) {
        try {
            File f = new File(".\\res\\8bit.txt");
            if (f.exists() && f.isFile()) {
                write(f);
                read(f);
            }
        } catch (NullPointerException ex) {
            System.out.println("Throwed an exception: " + ex.getMessage());
            //ex.notif
        }
    }
    
    private static void write(File f) {
        try {
            FileOutputStream f_out = new FileOutputStream(f);
            for (int i=0; i < 255; i++) {
                f_out.write(i);   
            }
            f_out.close();
        } catch (FileNotFoundException ex) {
            System.out.println("Throwed an exception: " + ex.getMessage());
        } catch (SecurityException ex) {
            System.out.println("Throwed an exception: " + ex.getMessage());
        } catch (IOException ex) {
            System.out.println("Throwed an exception: " + ex.getMessage());
        }
    }
    
    private static void read(File f) {
        try {
            FileInputStream f_in = new FileInputStream(f);
            int i = 0;
            while (i != -1) {
                i = f_in.read();
                System.out.print(i);
            }
            f_in.close();
        } catch (FileNotFoundException ex) {
            System.out.println("Throwed an exception: " + ex.getMessage());
        } catch (SecurityException ex) {
            System.out.println("Throwed an exception: " + ex.getMessage());
        } catch (IOException ex) {
            System.out.println("Throwed an exception: " + ex.getMessage());
        }
    }
    
}