package fileeightbits;

import java.io.File;

/**
 *
 * @author Valentin
 */
public class Tree {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            //args[0] = "C:\\";
            //File root = new File(args[0]);

            //File root = new File("C:\\");
            File root = new File("D:\\Valentin\\Documents\\Personal\\Code\\NetBeansProjects\\AplicacionTraductor1");
            int level = 0;
            tree(root, level);
            //System.out.print(tree(root, level));
        } catch(NullPointerException e) {
            System.out.println("Pathname argument is null");
        }
    }
    
    /**
     * @param parent the parent file (root folder to run tree)
     * @param level indentation level count for proper output
     */
    private static void tree(File parent, int level) {
        try {
            if (parent.isDirectory()) {
                File[] childs = parent.listFiles();
                if (level == 0) {
                    //System.out.print("Printing tree for " + parent.getName() + ":\n\n.\n");
                    System.out.print(" .\n");
                }
                for(File child : childs) {
                    printTree(child, level);
                    if(child.isDirectory()){
                        level++;
                        tree(child, level);
                        level--;
                    }
                }
            } else {
                System.out.println(parent.getName() + " is not a dir");
            }
        } catch(NullPointerException e) {
            System.out.println("Pathname argument is null");
        } catch(SecurityException e) {
            System.out.println("Access denied to the dir");
        }
    }
    
    private static void printTree(File child, int level) {
        try {
            String res = "";
            for (int i=0; i < level; i++) {
                res += "      ";
            }
            res += "└─ ";
            if (child.isDirectory()) {
                res += "[" + child.getName() + "]";
            } else {
                res += child.getName();
            }
            res += "\n";
            System.out.print(res);
        } catch(SecurityException e) {
            System.out.println("Access denied to the dir");
        }
    }

    

//// ONLY ONE FUNCTION AND RETURNS STRING
//    /**
//     * @param parent the parent file (root folder to run tree)
//     * @param level indentation level count for proper output
//     */
//    private static String tree(File parent, int level) {
//        try {
//            String res = "";
//            if (parent.isDirectory()) {
//                if (level == 0) {
//                    //System.out.print("Printing tree for " + parent.getName() + ":\n\n.\n");
//                    res += ".\n";
//                }
//                File[] childs = parent.listFiles();
//                for(File child : childs) {
//                    for (int i=0; i < level; i++) {
//                        res += "    ";
//                    }
//                    res += "└─ ";
//                    if (child.isDirectory()) {
//                        res += "[" + child.getName() + "]";
//                    } else {
//                        res += child.getName();
//                    }
//                    res += "\n";
//                    if(child.isDirectory()){
//                        level++;
//                        res += tree(child, level);
//                        level--;
//                    }
//                }
//            } else {
//                res += parent.getName() + " is not a dir";
//            }
//            return res;
//        } catch(NullPointerException e) {
//            System.out.println("Pathname argument is null");
//            return null;
//        } catch(SecurityException e) {
//            System.out.println("Access denied to the dir");
//            return null;
//        }
//    }
    
}