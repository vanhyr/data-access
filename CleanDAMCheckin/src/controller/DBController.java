package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.util.ArrayList;
import model.Checkin;

/**
 * Class to manage data from db.
 * @author Valentin Ros Duart
 */
public class DBController {
    
    private final Connection connection;
    
    /**
     * Constructor, initializes the connection.
     * @param odb
     */
    public DBController(OracleDB odb) {
        connection = odb.getConnection();
    }
    
    /**
     * Change the date format for the current db session to "dd/MM/yyyy hh24:mi".
     * -- No longer needed, the date its stored with time and date on checkin. --
     */
    /*public void changeDateFormat() {
        String query = "ALTER SESSION SET NLS_DATE_FORMAT = 'dd/MM/yyyy hh24:mi'";
        PreparedStatement prepStatement;
        try {
            // Prepare the statement
            prepStatement = connection.prepareStatement(query);
            try {
                // Execute the query
                prepStatement.executeUpdate();
            } catch (SQLTimeoutException ex) {
                System.out.println(ex.getSQLState());
            }
            prepStatement.close();
        } catch (SQLException ex) {
            System.out.println(ex.getSQLState() + ex.getMessage());
        }
    }*/
    
    /**
     * Create a new checkin in the db.
     * @param checkin the chekin object.
     * @return the number of affected rows.
     */
    public int insertCheckin(Checkin checkin) {
        int affectedRows = 0;
        // fecHoraFin null to indicate is not closed, just opened
        String query = "INSERT INTO fichaje VALUES (?, ?, TO_DATE (?, 'dd/MM/yyyy hh24:mi'), null)";
        PreparedStatement prepStatement;
        try {
            // Prepare the statement
            prepStatement = connection.prepareStatement(query);
            // Give it the ?  values
            prepStatement.setInt(1, checkin.getId());
            prepStatement.setString(2, checkin.getDni());
            prepStatement.setString(3, checkin.getEntrance());
            try {
                // Execute the query
                affectedRows = prepStatement.executeUpdate();
            } catch (SQLTimeoutException ex) {
                System.out.println(ex.getSQLState());
            }
            // Close the prepared statement
            prepStatement.close();
        } catch (SQLException ex) {
            System.out.println(ex.getSQLState() + ex.getMessage());
        }
        return affectedRows;
    }
    
    /**
     * Update the checkin in the db in order to close it.
     * @param checkin the chekin object.
     * @return the number of affected rows.
     */
    public int closeChekin(Checkin checkin) {
        int affectedRows = 0;
        // Only set the fecHoraFin (exit)
        String query = "UPDATE fichaje SET fecHoraFin = TO_DATE (?, 'dd/MM/yyyy hh24:mi') WHERE dni = ?";
        PreparedStatement prepStatement;
        try {
            // Prepare the statement
            prepStatement = connection.prepareStatement(query);
            // Give it the ?  value (the tableName)
            prepStatement.setString(1, checkin.getExit());
            prepStatement.setString(2, checkin.getDni());
            try {
                // Execute the query
                affectedRows = prepStatement.executeUpdate();
            } catch (SQLTimeoutException ex) {
                System.out.println(ex.getSQLState());
            }
            // Close the prepared statement
            prepStatement.close();
        } catch (SQLException ex) {
            System.out.println(ex.getSQLState() + ex.getMessage());
        }
        return affectedRows;
    }
    
    /**
     * Get the active checkins from the db (the ones that doesn't have exit date).
     * @return active checkins ArrayList.
     */
    public ArrayList<Checkin> getActiveCheckins() {
        ArrayList<Checkin> activeCheckins = new ArrayList<>();
        // Get all the checkin with fecHoraFin = null meaning that checkin is open
        String query = "SELECT * FROM fichaje WHERE fecHoraFin IS null";
        PreparedStatement prepStatement;
        try {
            // Prepare the statement
            prepStatement = connection.prepareStatement(query);
            try {
            // Execute the query
            ResultSet rs = prepStatement.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String dni = rs.getString(2);
                String entrance = rs.getString(2);
                String exit = rs.getString(3);
                activeCheckins.add(new Checkin(id, dni, entrance, exit));
            }
            // Close resultSet
            rs.close();
            } catch (SQLTimeoutException ex) {
                System.out.println(ex.getSQLState());
            }
            // Close the prepared statement
            prepStatement.close();
        } catch (SQLException ex) {
            System.out.println(ex.getSQLState() + ex.getMessage());
        }
        return activeCheckins;
    }
    
    /**
     * Get the last checkin id from the checkin table in the db.
     * @return the last id.
     */
    public int getLastCheckinId() {
        int lastId = 0;
        // Get all the checkin with fecHoraFin = null meaning that checkin is open
        String query = "SELECT MAX(idFichaje) FROM fichaje";
        PreparedStatement prepStatement;
        try {
            // Prepare the statement
            prepStatement = connection.prepareStatement(query);
            try {
            // Execute the query
            ResultSet rs = prepStatement.executeQuery();
            while (rs.next()) {
                lastId = rs.getInt(1);
            }
            // Close resultSet
            rs.close();
            } catch (SQLTimeoutException ex) {
                System.out.println(ex.getSQLState());
            }
            // Close the prepared statement
            prepStatement.close();
        } catch (SQLException ex) {
            System.out.println(ex.getSQLState() + ex.getMessage());
        }
        return lastId;
    }
    
}
