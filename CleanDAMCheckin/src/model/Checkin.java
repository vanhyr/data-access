package model;

import util.FormatValidator;

/**
 * Model class for the db Checkin table.
 * @author Valentin Ros Duart.
 */
public class Checkin {
    
    private int id;
    private String dni, entrance, exit;

    public Checkin() {
        
    }

    public Checkin(int id, String dni, String entrance, String exit) {
        this.id = id;
        this.dni = dni;
        this.entrance = entrance;
        this.exit = exit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        if (id > 0) {
            this.id = id;
        }
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        if (FormatValidator.isValidDni(dni)) {
            this.dni = dni;
        }
    }

    public String getEntrance() {
        return entrance;
    }

    public void setEntrance(String entrance) {
        if (FormatValidator.isValidLocalDateTime(entrance)) {
            this.entrance = entrance;
        }
    }

    public String getExit() {
        return exit;
    }

    public void setExit(String exit) {
        if (FormatValidator.isValidLocalDateTime(exit)) {
            this.exit = exit;
        }
    }

    @Override
    public String toString() {
        return "Checkin{" + "id=" + id + ", dni=" + dni + ", entrance=" + entrance + ", exit=" + exit + "}";
    }
    
}
