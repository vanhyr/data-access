package util;

/**
 * Constant credentials for the oracle db connection.
 * @author Valentin Ros Duart.
 */
public class Credentials {
    
    public static final String SERVER = "localhost";
    public static final int PORT = 1521;
    public static final String ID = "xe";
    public static final String USER = "AD_TEMA02_FICHAJES";
    public static final String PASS = "AD_TEMA02_FICHAJES";
    
}
