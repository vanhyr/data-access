package controller;

import java.util.ArrayList;

/**
 * Class Student.
 * @author Valentin Ros Duart
 */
public class Student {
    
    private int id;
    private String name;
    private double grade;
    private boolean repeating;

    /**
     * Create a new Student.
     * @param id student's id number
     * @param name student's name
     * @param grade student's grade
     * @param repeating student's repeating state
     */
    public Student(int id, String name, double grade, boolean repeating) {
        // Check for not allowed values before setting the parameters
        if(id >= 1 && !name.isEmpty() && name.length() <= 50 && grade >= 0 && grade <= 10) {
            this.id = id;
            this.name = name;
            this.grade = grade;
            this.repeating = repeating;
        }
    }
    
    /**
     * Create a new Student.
     * @param id student's id number
     * @param name student's name
     * @param grade student's grade
     */
    public Student(int id, String name, double grade) {
        // Check for not allowed values before setting the parameters
        if(id >= 1 && !name.isEmpty() && name.length() <= 50 && grade >= 0 && grade <= 10) {
            this.id = id;
            this.name = name;
            this.grade = grade;
            this.repeating = false;
        }
    }
    
    /**
     * Get the student's id.
     * @return id int
     */
    public int getId() {
        return id;
    }

    /**
     * Set the student's id.
     * @param id int
     */
    public void setId(int id) {
        // Only if it is greater than one
        if(id >= 1) {
            this.id = id;
        }
    }

    /**
     * Get the student's name.
     * @return name String
     */
    public String getName() {
        return name;
    }

    /**
     * Set the student's name.
     * @param name String
     */
    public void setName(String name) {
        // Only if it is not empty and is lower than 50 chars, to prevent it is
        // higher than 100 bytes (to work properly with laterrandom access file structure)
        if(!name.isEmpty() && name.length() <= 50) {
            this.name = name;
        }
    }

    /**
     * Get the student's grade.
     * @return grade double
     */
    public double getGrade() {
        return grade;
    }

    /**
     * Set the student's grade.
     * @param grade double
     */
    public void setGrade(double grade) {
        // Only if the grade is a valid one (>=0 and <=10)
        if(grade >= 0 && grade <= 10) {
            this.grade = grade;
        }
    }

    /**
     * Get the student's repeating state.
     * @return age int
     */
    public boolean isRepeating() {
        return repeating;
    }

    /**
     * Set the student's repeating state.
     * @param repeating boolean
     */
    public void setRepeating(boolean repeating) { 
        this.repeating = repeating;
    }

    /**
     * Converts the student object to an ArrayList with its parameters.
     * @return student ArrayList
     */
    public ArrayList toArrayList(){
        ArrayList student = new ArrayList();
        student.add(id);
        student.add(name);
        student.add(grade);
        student.add(repeating);
        return student;
    }
    
    /**
     * Converts the student object to a String with its parameters.
     * @return student's String
     */
    @Override
    public String toString() {
        return "Alumno{" + "id=" + id + ", name=" + name + ", grade=" + grade + ", repeating=" + repeating + '}';
    }
    
}
