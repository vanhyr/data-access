package controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Class RafManager for managing random access files and its exceptions.
 * @author Valentin Ros Duart
 */
public class RafManager {
    
    private File file;
    private RandomAccessFile raf;

    /**
     * Create a new RAFManager.
     * @param uri
     */
    public RafManager(String uri) {
        // It handles initializating the RandomAcessFile in read/write mode
        // and the file where it will write/read.
        open(uri);
    }
    
    /**
     * Create a new RAFManager.
     * @param folderUri
     * @param fileName
     */
    public RafManager(String folderUri, String fileName) {
        // It handles initializating the RandomAcessFile in read/write mode
        // and the file where it will write/read.
        open(folderUri, fileName);
    }
    
    /**
     * Get the file.
     * @return File
     */
    public File getFile() {
        return file;
    }
    
    /**
     * Get the raf.
     * @return RandomAccessFile
     */
    public RandomAccessFile getRaf() {
        return raf;
    }
    
    /**
     * Sets the file to later interact with the raf.
     * @param uri the URI String
     */
    private void openFile(String uri) {
        try {
            // Create a file object for the file
            File file = new File(uri);
            // Get it's parent (folder)
            File folder = new File(file.getParentFile().getAbsolutePath());
            // Only if that folder exists and it's a directory
            if (folder.exists() && folder.isDirectory()) {
                // Create a new file object with the given folder and fileName
                file = new File(folder, file.getName());
                // Sets the file class variable to the given file
                this.file = file;
            }
        } catch(NullPointerException | SecurityException ex) {
            System.out.println(ex.getMessage());
        }
    }
    
    /**
     * Sets the file to later interact with the raf.
     * @param parentUri the folder URI String
     * @param fileName the file name String
     */
    private void openFile(String parentUri, String fileName) {
        try {
            // Create a file object for the folder
            File folder = new File(parentUri);
            // Only if that folder exists and it's a directory
            if (folder.exists() && folder.isDirectory()) {
                // Create a new file object with the given folder and fileName
                file = new File(folder, fileName);
                // Sets the file class variable to the given file
                this.file = file;
            }
        } catch(NullPointerException | SecurityException ex) {
            System.out.println(ex.getMessage());
        }
    }
    
    /**
     * First sets the file and then initializes the raf.
     * @param uri the URI String
     */
    private void open(String uri) {
        // Calls openFile method with the given uri. It sets the file class
        // variable from that uri.
        openFile(uri);
        try {
            try {
                // Creates a RandomAccessFile object from the file class
                // variable with read/write permissions
                raf = new RandomAccessFile(file, "rw");
            } catch (FileNotFoundException ex) {
                System.out.println(ex.getMessage());
            }
        } catch(NullPointerException | SecurityException ex) {
            System.out.println(ex.getMessage());
        }
    }
    
    /**
     * First sets the file and then initializes the raf.
     * @param folderUri the folder URI String
     * @param fileName the file name String
     */
    private void open(String folderUri, String fileName) {
        openFile(folderUri, fileName);
        try {
            try {
                raf = new RandomAccessFile(file, "rw");
            } catch (FileNotFoundException ex) {
                System.out.println(ex.getMessage());
            }
        } catch(NullPointerException | SecurityException ex) {
            System.out.println(ex.getMessage());
        }
    }
    
    /**
     * Closes the raf.
     */
    public void close(){
        try {
            try {
                raf.close();
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        } catch(NullPointerException | SecurityException ex) {
            System.out.println(ex.getMessage());
        }
    }
    
}
