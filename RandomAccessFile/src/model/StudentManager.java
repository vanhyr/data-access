package model;

import controller.RafManager;
import controller.Student;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class StudentManager which manage the connection operations between students and the random access file (db file)
 * @author Valentin Ros Duart
 */
public class StudentManager {
    
    private RafManager rafm;
    // Static for the size that each register occupies
    private static int registerSize = 113;
    // Static for the size of the name String (in chars)
    private static int nameSize = 50;
    
    /**
     * Create a new StudentManager.
     * @param uri the uri for the file
     */
    public StudentManager(String uri) {
        // Initializes a RafManager object with the given folder URI
        // and file name. It handles initializating the RandomAcessFile
        // in read/write mode and the file where it will write/read.
        rafm = new RafManager(uri);
    }
    
    /**
     * Create a new StudentManager.
     * @param folderUri the uri of the folder
     * @param fileName the name of the file
     */
    public StudentManager(String folderUri, String fileName) {
        // Initializes a RafManager object with the given folder URI
        // and file name. It handles initializating the RandomAcessFile
        // in read/write mode and the file where it will write/read.
        rafm = new RafManager(folderUri, fileName);
    }
    
    /**
     * Get the RafManager to later operate with the random access file.
     * @return RafManager
     */
    public RafManager getRafm() {
        return rafm;
    }
    
    /**
     * Writes the given student into a file using RandomAcessFile.
     * @param student
     * @return 
     * true if it could write it into the file.
     * false if it couldn't write it into the file.
     */
    public boolean write(Student student){
        try {
            // Get the initialized RandomAccessFile from the RafManager
            RandomAccessFile raf = rafm.getRaf();
            try {
                // Creates the file if the file is not a dir and it doesn't exists
                if(!rafm.getFile().isDirectory() && !rafm.getFile().exists()) {
                    rafm.getFile().createNewFile();
                }
            } catch (SecurityException | IOException ex) {
                System.out.println(ex.getMessage());
            }
            // Calc the position: (id - 1) * (its attributes size in bytes)
            // (id - 1) * id(4 bytes) + name(100 bytes) + grade(8 bytes) + repeater(1 byte)
            // (id -1) * 113
            int pos = (student.getId() - 1) * registerSize ;
            try {
                // Places the file pointer to the position calculated before
                raf.seek(pos);
                // Write the id and move the pointer 4 bytes
                raf.writeInt(student.getId());
                // Stores the name in a buffer of 50 chars (100 bytes) to make it's always 
                // the same length for consistency in the file
                StringBuffer name = new StringBuffer(student.getName());
                name.setLength(nameSize);
                // Write the name and move the pointer 100 bytes
                raf.writeChars(name.toString());
                // Write the grade and move the pointer 8 bytes
                raf.writeDouble(student.getGrade());
                // Write repeating and move the pointer 1 byte
                raf.writeBoolean(student.isRepeating());
                return true;
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        } catch (NullPointerException ex) {
            System.out.println(ex.getMessage());
        }
        return false;
    }
    
    /**
     * Removes the given student from its id from the file using RandomAcessFile.
     * @param id
     * @return 
     * true if it could logical remove it from the file.
     * false if it couldn't logical remove it from the file.
     */
    public boolean delete(int id){
        try {
            // Get the initialized RandomAccessFile from the RafManager
            RandomAccessFile raf = rafm.getRaf();
            try {
                // Only if the file exists
                if (rafm.getFile().exists()) {
                    // Calc the position: (id - 1) * (its attributes size in bytes)
                    // (id - 1) * id(4 bytes) + name(100 bytes) + grade(8 bytes) + repeater(1 byte)
                    // (id -1) * 113
                    int pos = (id - 1) * registerSize ;
                    // For storing the key register value
                    int reg;
                    try {
                        // Places the file pointer to the position calculated before
                        raf.seek(pos);
                        try {
                            // Store the key value of the register into reg and
                            // move the pointer 4 bytes
                            reg = raf.readInt();
                        } catch(IOException ex) {
                            // Set the key value of the register to 0 
                            // (deleted/not exists) if there is an IOException
                            reg = 0;
                        }
                        // If the reg is 0 (deleted/not exists)
                        if (reg == 0) {
                            return false;
                        } else {
                            // Put the pointer back into the firts position
                            raf.seek(pos);
                            // Write a 0 into the key value and move the
                            // pointer 4 bytes
                            raf.writeInt(0);
                            return true;
                        }
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                }
            } catch(SecurityException ex) {
                System.out.println(ex.getMessage());
            }
        } catch (NullPointerException ex) {
            System.out.println(ex.getMessage());
        }
        return false;
    }
    
    /**
     * Read the given student from its id from the file using RandomAcessFile.
     * @param id
     * @return 
     * true if it could logical read it from the file.
     * false if it couldn't logical read it from the file.
     */
    public Student read(int id){
        try {
            // Get the initialized RandomAccessFile from the RafManager
            RandomAccessFile raf = rafm.getRaf();
            try {
                // Only if the file exists
                if (rafm.getFile().exists()) {
                    // Calc the position: (id - 1) * (its attributes size in bytes)
                    // (id - 1) * id(4 bytes) + name(100 bytes) + grade(8 bytes) + repeater(1 byte)
                    // (id -1) * 113
                    int pos = (id - 1) * registerSize ;
                    // For storing the key register value
                    int reg;
                    try {
                        // Places the file pointer to the position calculated before
                        raf.seek(pos);
                        try {
                            // Store the key value of the register into reg and
                            // move the pointer 4 bytes
                            reg = raf.readInt();
                        } catch(IOException ex) {
                            // Set the key value of the register to 0 
                            // (deleted/not exists) if there is an IOException
                            reg = 0;
                        }
                        // If the reg is 0 (deleted/not exists)
                        if (reg == 0) {
                            return null;
                        } else {
                            // Create a new array of chars with length 50
                            // (100 bytes) to make it's always the same length 
                            // for consistency in the file
                            char[] charName = new char[nameSize];
                            // Iterates 50 times (chars in the array)
                            for(int i = 0; i < nameSize; i++) {
                                // Read a char for each iteration and move the
                                // pointer 2 bytes (100 in total)
                                charName[i] = raf.readChar();
                            }
                            String name = new String(charName);
                            // Read the grade and move the pointer 8 bytes
                            double grade = raf.readDouble();
                            // Read repeating and move the pointer 1 byte
                            boolean repeating = raf.readBoolean();
                            return new Student(id, name, grade, repeating);
                        }
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                }
            } catch(SecurityException ex) {
                System.out.println(ex.getMessage());
            }
        } catch (NullPointerException ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }
    
    /**
     * Read all students but removed ones from the file using RandomAcessFile.
     * @return ArrayList of students
     */
    public ArrayList readAll() {
        // Initialize the ArrayList for later return
        ArrayList<Student> students = new ArrayList<>();
        try {
            // Get the initialized RandomAccessFile from the RafManager
            RandomAccessFile raf = rafm.getRaf();
            try {
                // Only if the file exists
                if (rafm.getFile().exists()) {
                    // For storing the key register value
                    int reg;
                    // Pointer to 0 (prevent posible errors)
                    raf.seek(0);
                    // Iterate until the pointer gets to the end of the file
                    for(int i = 0; raf.getFilePointer() < raf.length(); i++) {
                        try {
                            // Place at the i element register
                            raf.seek(i * registerSize);
                            try {
                                // Store the key value of the register into reg and
                                // move the pointer 4 bytes
                                reg = raf.readInt();
                            } catch(IOException ex) {
                                // Set the key value of the register to 0 
                                // (deleted/not exists) if there is an IOException
                                reg = 0;
                            }
                            // If the reg is not 0 (not deleted/exists)
                            // Basically ignore them
                            if (reg != 0) {
                                int id = reg;
                                // Create a new array of chars with length 50
                                // (100 bytes) to make it's always the same length 
                                // for consistency in the file
                                char[] charName = new char[nameSize];
                                // Iterates 50 times (chars in the array)
                                for(int j = 0; j < nameSize; j++) {
                                    // Read a char for each iteration and move the
                                    // pointer 2 bytes (100 in total)
                                    charName[j] = raf.readChar();
                                }
                                String name = new String(charName);
                                // Read the grade and move the pointer 8 bytes
                                double grade = raf.readDouble();
                                // Read repeating and move the pointer 1 byte
                                boolean repeating = raf.readBoolean();
                                students.add(new Student(id, name, grade, repeating));
                            }
                        } catch (IOException ex) {
                            System.out.println(ex.getMessage());
                        }
                    }
                }
            } catch(SecurityException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                Logger.getLogger(StudentManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch(NullPointerException ex) {
            System.out.println(ex.getMessage());
        }
        return students;
    }
    
    /**
     * Iterates the file using RandomAcessFile to check if the student with the
     * given id exists.
     * @return
     * true if it could find the id in the file.
     * false if it couldn't find the id in the file.
     */
    public boolean exists(int id) {
        try {
            // Get the initialized RandomAccessFile from the RafManager
            RandomAccessFile raf = rafm.getRaf();
            try {
                // Only if the file exists
                if (rafm.getFile().exists()) {
                    // For storing the key register value
                    int reg;
                    // Pointer to 0 (prevent posible errors)
                    raf.seek(0);
                    // Iterate until the pointer gets to the end of the file
                    for(int i = 0; raf.getFilePointer() < raf.length(); i++) {
                        try {
                            // Place at the i element register
                            raf.seek(i * registerSize);
                            try {
                                // Store the key value of the register into reg and
                                // move the pointer 4 bytes
                                reg = raf.readInt();
                            } catch(IOException ex) {
                                // Set the key value of the register to 0 
                                // (deleted/not exists) if there is an IOException
                                reg = 0;
                            }
                            // If the reg is not 0 (not deleted/exists)
                            // Basically ignore them
                            if (reg != 0) {
                                // If the key value of the register equals the given id
                                if(reg == id) {
                                    // exit from the search and set it to true
                                    return true;
                                }
                            }
                        } catch (IOException ex) {
                            System.out.println(ex.getMessage());
                        }
                    }
                }
            } catch(SecurityException ex) {
                System.out.println(ex.getMessage());
            } catch (IOException ex) {
                Logger.getLogger(StudentManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch(NullPointerException ex) {
            System.out.println(ex.getMessage());
        }
        return false;
    }
    
}
