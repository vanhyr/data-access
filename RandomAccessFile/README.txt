Hola Pepe, mi programa de escritura aleatoria en archivos funciona de la siguiente manera:

- Cuando lanzas la app el archivo que se crea por defecto se llama students.db y se pone en la 
  carpeta "res" dentro de la estructura de carpetas del proyecto.
- Se puede cambiar la carpeta mediante el botón "folder" que muestra un JFileChooser.
- Se puede cambiar el nombre del archivo mediante el input (arriba a la izq) dándole al botón "Set"
  o pulsando "Intro".
  - Si se deja en blanco siempre vuelve a colocar el archivo por defecto (students.db)
- El stream de flujo de datos se cierra o bien si cambias la carpeta, el nombre del archivo o
  cierras la app.
- Se puede filtrar la tabla por id con el input  (arriba a la derecha) dándole al botón "Filter by id"
  o pulsando "Intro"
  - Es importante saber que si meto "1", mostrará todos lo que contengan un 1 (p.e: 1, 14 ,118, etc.)
- El campo grade (nota) admite solo coma como separador de número decimal (p.e: 1,2 funciona / 1.2 lo rechaza)

Un saludo, Valentín Ros Duart.