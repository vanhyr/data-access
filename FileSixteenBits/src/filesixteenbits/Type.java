package filesixteenbits;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * Class Type, mimics the MS-DOS TYPE command.
 * @author Valentin Ros Duart
 */
public class Type {
    
    public static FileReader fr;

    /**
     * Main
     * @param args command line args
     * @throws FileNotFoundException 
     */
    public static void main(String[] args) throws FileNotFoundException {
        System.out.print(type(args));
    }
     
    /**
     * Types file content.
     * @param args command line args
     * @return (String) The file content.
     */
    private static String type(String[] args) {
        String output = "";
        File f;
        try {
            if(args.length == 0) {
                f = new File("src\\filesixteenbits\\Type.java");
            } else {
                String path = args[0];
                f = new File(path);
            }
            if (f.exists()) {
                fr = new FileReader(f);
                int asci;
                asci = fr.read();
                while(asci != -1) {
                    output += (char) asci;
                    asci = fr.read();
                }
                fr.close();
            }
            return output;
        } catch(FileNotFoundException | NullPointerException ex) {
            System.out.println("Error: " + ex.getMessage());
        } catch (IOException ex) {
            Logger.getLogger("Error: " + ex.getMessage());
        }
        return output;
    }
    
}
