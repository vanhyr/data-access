package filesixteenbits;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

/**
 *
 * @author Valentin
 */
public class Encrypt {
    
    public static FileReader fr;
    public static FileWriter fw;
    
    /**
     * Main
     * @param args command line args
     */
    public static void main(String[] args) {
        if(encrypt(args) && args.length > 0) {
            System.out.println("File " + args[0] + " encrypted.");
        }
    }
    
    /**
     * Encrypts file content.
     * @param args command line args
     * @return true if succeeds false if it doesn't
     */
    private static boolean encrypt(String[] args) {
        String read = "";
        File file;
        try {
            if(args.length == 0) {
                file = new File("src\\filesixteenbits\\Copy.cp");
            } else {
                String path = args[0];
                file = new File(path);
            }
            fr = new FileReader(file);
            Random rand = new Random();
            int asci;
            asci = fr.read();
            asci += rand.nextInt(255 - asci);
            while(asci != -1) {
                read += (char) asci;
                asci = fr.read();
                asci += rand.nextInt(255 - asci);
            }
            fr.close();
            fw = new FileWriter(file);
            fw.write(read);
            fw.close();
            return true;
        } catch(FileNotFoundException | NullPointerException | SecurityException ex) {
            System.out.println("Error: " + ex.getMessage());
            return false;
        } catch (IOException ex) {
            System.out.println("Error: " + ex.getMessage());
            return false;
        }
    }
    
}
